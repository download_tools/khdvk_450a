# 烧录工具介绍

本文档提供两种烧录方法，一种是GD-link，另一种是串口烧录工具。串口烧录工具只能在windows环境下运行，在用串口烧录工具前必须先把bootloader烧录进开发板，现在bootloader已经整合进`gd32f4xx.bin`，所以面对新的开发板， **必须** 先用GD-Link烧录一遍gd32f4xx.bin。串口烧录工具有个优势是可以 **同时烧录多个开发板** 。

## 通过GD-Link烧录

### 1.烧录前准备

1.如下图，连接电源线和烧录线，进行此种烧录可以不用串口线

![IMG_20221017_160158](figures/IMG_20221017_160158.jpg)

2.下载GD-Link

[下载链接](https://www.gd32mcu.com/data/documents/toolSoftware/GD_Link_Programmer_V4.6.10.13769.7z)

### 2.烧录流程

1.GD-Link连接开发板

![image-20221017162233535](figures/image-20221017162233535.png)

2.打开`\out\khdvk_450a\khdvk_450a`下的`gd32f4xx.bin`，该bin文件包含了`bootloader`，`OHOS_Image.bin`和`lfs.image`。

![image-20221017162540788](figures/image-20221017162540788.png)

3.输入起始地址，这里就选择默认的`8000000`，然后点击OK。

![image-20221017162641538](figures/image-20221017162641538.png)

4.点击`Program`，即可开始烧录。

![image-20221017162816133](figures/image-20221017162816133.png)

4.烧录完成。可能会出现”校验错误“，但是没什么影响。因为`gd32f4xx.bin`较大，有3MB，所以烧录时间较长。

![image-20221017163341945](figures/image-20221017163341945.png)

### 只烧录OHOS_Image.bin方法

分为两种情况，一种是保留bootloader和lfs.image；一种是删除bootloader和lfs.image。

1.保留bootloader和lfs.image

查看`device/board/kaihong/khdvk_450a/liteos_m/bsp/startup/gd32f4xx_flash.ld`

![image-20221018143147824](figures/image-20221018143147824.png)

注意到此ORIGIN为`0x8010000`，那么我们在烧录流程的第三步，填写**初始地址**为`0x8010000`，当然第二步的文件打开要变为`OHOS_Image.bin`

2.删除bootloader和lfs.image

将`device/board/kaihong/khdvk_450a/liteos_m/bsp/startup/main.c`中的`InitBoardBefore();`注释掉，如图

![image-20221018143956305](figures/image-20221018143956305.png)

并且将`device/board/kaihong/khdvk_450a/liteos_m/bsp/startup/gd32f4xx_flash.ld`中的ORIGIN改为`0x8000000`，

修改位置如下图。

![image-20221018143147824](figures/image-20221018143147824.png)

然后的流程就如烧录流程一样，只是第二步要将`gd32f4xx.bin`，变为`OHOS_Image.bin`。

## 通过串口烧录工具烧录

用串口烧录工具前，必须保证开发板中有`bootloader`，就是必须之前烧录过`gd32f4xx.bin`，按一下RESET按钮，开发板如果有灯闪3下，就表明有`bootloader`。

### 1.烧录前准备

#### 1.连接线路

如下图，连接电源线、烧录线，和串口线

![IMG_20221017_160158](figures/IMG_20221017_160158.jpg)

#### 2.工具下载

本目录下的burn_tool文件夹就是串口烧录工具

#### 3.配置python环境

##### 1.下载python程序

登陆python官网https://www.python.org/，选择download页面进去找到python的一个版本下载安装，我用的是python3.10.7。记住要选中”Add Python 3.10 to PATH"，这样就不用我们自己配置环境变量了。这样就可以直接跳到第四步。

![image-20221009111310489](figures/image-20221009111310489.png)



##### 2.配置python环境变量

找到安装位置，python.exe就是python的解释器，之前在cmd里输入python无法执行，就是因为cmd在默认路径中找不到python命令。我们复制这个路径。

![image-20221009104422387](figures/image-20221009104422387.png)

找到”系统变量“里面的”Path“，编辑”Ptah“，然后再点击”新建“,将刚才的路径粘贴上去

![image-20221009104739979](figures/image-20221009104739979.png)

点击”确定“以保存配置。然后在cmd命令行界面中输入`python -v`，可看到如下页面表示python环境变量配置完成。

![image-20221009105745645](figures/image-20221009105745645.png)

##### 3.配置pip环境变量

复制pip路径

![image-20221009111141949](figures/image-20221009111141949.png)

将pip路径也加入到环境变量里。

##### 4.安装必要的库

安装`pyserial`。(安装`serial`不行，会报没有`serial.tools`模块错误)

```
pip install pyserial
```

### 2.使用方法：

​	可以在 `CMD` 下键入 `python main.py -h` 或者 `./main.py -h` 查看帮助命令，帮助提示如下：

```
The use options of class_serial.py are as follows:
   main.py -f <filename> -c <com_port1> <com_port2> <com_port3> ......
   or: class_serial.py --filename=<filename> --com_port=<com_port1> <com_port2> <com_port3> ......
   You can user class_serila.py -p  or  class_serial --port for looking the com port in computer.
   You can user class_serial.py -t <text-number> or class_serial.py --text=<text-number> user to text the process.
   If you want to user test , please user the order:
       class_serial.py -f <filename> -t <text-number> -c <com_port1> <com_port2> <com_port3> ......
       the <text-number> is the time of testing restart and process ......
```

#### 使用示例

串口线链接开发板和主机之后，可在设备管理器-->端口中查看该开发板对应的端口。

输入命令之后，必须要按一下开发板上的**RESET按钮**，这样开发板上的运行程序才会由OHOS变成bootloader。

下例是将OHOS_Image.bin通过com26串口烧录到开发板

```
python main.py -f OHOS_Image.bin -c com26 
```

下例是同时烧录两块板子，分别通过com26串口和com29串口

```
python main.py -f OHOS_Image.bin -c com26 com29
```

下例是将OHOS_Image.bin通过com26串口烧录到开发板，烧录两次

```
python main.py -f OHOS_Image.bin -t 2 -c com26  
```

运行烧录工具之后会在当前目录下生成日志文件，记录了烧录工具与开发板上的bootloader的握手过程。

### 3.不能正常烧录可能的原因

1.文件名不对

文件名不对的话，会报相应错误

2.串口未打开

注意串口烧录工具只能在window下使用，不能在linux中使用。

在设备管理器-->端口中查看该端口是否存在，

如果存在，再检查是否有其它设备占用端口，比如SSCOM、XCOM。

3.串口线连接方式

如果是用pin引脚配置的串口可能出现问题（如开发板只能发送数据，不能接收数据），建议用[连接线路](#1.连接线路)所示连接串口线。

