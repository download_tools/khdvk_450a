import datetime
import threading
class Write_log():
    def __init__(self,file_name):
        self.fd=open(file_name,'w')
    def write_log(self,str):
        self.fd.write(str)
        self.fd.flush()

def write_time_to_file(port,cost_time):
    file_name="time_record.log"
    fd=open(file_name,'a')
    now_time = datetime.datetime.now().strftime('%F %T')
    content="{} {} transmission cost time {}s \n".format(now_time,port,cost_time)
    fd.write(content)
    fd.close()
