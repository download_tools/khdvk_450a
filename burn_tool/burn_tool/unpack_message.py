import struct
class unpack_message():                     #解包函数只需要解开收到的包即可
    def __init__(self,message):
        #print("解包传输信息块——————————")
        self.message=message

    def get_unpack_message(self):
        s=struct.Struct('5B')                                  #0x 05 05 0A 0A code 五个字节
        unpack_data=s.unpack(self.message)
        return unpack_data

    def get_upacked_file(self):
        return self.message[7:-1]

    def updata(self,message):
        self.message=message
