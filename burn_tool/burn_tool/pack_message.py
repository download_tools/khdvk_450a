import  struct
class pack_message():
    def __init__(self,code,message,msg_len):
        #print("打包传输信息块——————————")
        self.code=code
        self.message=message
        self.len=msg_len
        self.buf=[0 for i in range(self.len+8)]
        self.buf[0]=0x05;self.buf[1]=0x05
        self.buf[2]=0x0A;self.buf[3]=0x0A
        self.buf[4]=self.code;self.buf[5]=(self.len+8)>>8
        self.buf[6]=(self.len+8)&0xff;self.buf[self.len+7]=0x00            #  0x 05 05 0A 0A code len*2 context crc

    def crc_generte(self):
        #print("文件块校验函数crc校验正在生成中")
        for i in range(self.len):
            self.buf[i+7]=self.message[i]
        #生成crc校验
        crc_temp=0x00
        for i in range(self.len+7):
            crc_temp=(crc_temp+self.buf[i])&0xff
        #print(crc_temp)
        self.buf[self.len+7]=(~crc_temp)&0xff           #将生成的校验块加入

    def file_crc_generate(self,filename):       # crc文件校验生成函数
        print("文件校验函数crc校验生成中")

    def get_pack_type(self):                      #返回struct类型需要打包的字节数量
        return str(self.len+8)+'B'

    def get_packed_message(self):        #先用最简单的那种
        self.crc_generte()
        #print(self.get_pack_type())
        s=struct.Struct(self.get_pack_type())
        packed_data=s.pack(*self.buf)
        #print(packed_data)
        return packed_data

    def updata(self,code,message,msg_len):
        self.code=code
        self.message=message
        self.len=msg_len
        self.buf=None                     #初始化然后重新设定传入的数据
        self.buf=[0 for i in range(self.len+8)]
        self.buf[0]=0x05;self.buf[1]=0x05
        self.buf[2]=0x0A;self.buf[3]=0x0A
        self.buf[4]=self.code;self.buf[5]=(self.len+8)>>8
        self.buf[6]=(self.len+8)&0xff;self.buf[self.len+7]=0x00            #  0x 05 05 0A 0A code len*2 context crc
