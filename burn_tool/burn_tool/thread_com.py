import threading
from serial_rec_send import serial_rec_send
from progress_bar import LineProgress, progress_bar_manager
from os import path
from math import ceil
FILE_BLOCK_SIZE=512        #定义文件读取每次读取的文件块大小
com="COM2"                 #发送串口的设置
bit=115200                 #设置发送的波特率                         No,这里设置的时候就需要设置超时时间 timeout = 直接把自己设置的超时时间写上即可......
timeout=0.5                #设置读取buffer的超时时间              通过设置buffer大小和timeout=None来控制读入五个字符继续进行，这个是可以的......可以将timeo
class thread_com():                    #实现多线程的串口传输协议，同时往多个串口传输文件
    def __init__(self,temp_args,filename):
        #print("实现多线程的串口传输协议")
        self.thread_serial=[]
        self.threads=[]
        self.file_block_num=ceil(path.getsize(filename)/FILE_BLOCK_SIZE)#ceil向上取整
        for i in temp_args:
            try:
                progress_bar_manager.put(i, LineProgress(total=self.file_block_num, title=i))
                self.thread_serial.append(serial_rec_send(i,bit,timeout))        #将serial端口加入线程中
            except Exception as e:
                print("the com port {} is not opened".format(i))
        for i in self.thread_serial:
            self.threads.append(threading.Thread(target=i.file_send,args=(filename,0)))

    def begin(self):
        for i in self.threads:
            i.start()


